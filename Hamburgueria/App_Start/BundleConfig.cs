﻿using System.Web;
using System.Web.Optimization;

namespace Hamburgueria
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.validate.min.js",
                        "~/Scripts/jquery.validate.unobtrusive.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/globalization").Include(
            "~/Scripts/globalize/globalize.js",
            "~/Scripts/globalize/cultures/globalize.culture.pt-BR.js"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                    "~/Scripts/siteHamburgueria.js",
                    "~/Scripts/jsapi.js",
                    "~/Scripts/app.js",
                  "~/Scripts/jquery.maskedinput.min.js",
                    "~/Scripts/jquery.maskMoney.min.js",
                    "~/Scripts/graficos.js",
                    "~/Scripts/alertify.min.js",
                    "~/Scripts/alertify.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                      "~/Scripts/Site/siteHamburgueria.js",
                      "~/Scripts/Site/modernizr-2.6.2-respond-1.1.0.min.js",
                      "~/Scripts/Site/jquery.easing.1.3.js",
                      "~/Scripts/Site/jquery.isotope.min.js",
                      "~/Scripts/Site/jquery.nicescroll.min.js",
                      "~/Scripts/Site/fancybox/jquery.fancybox.pack.js",
                      "~/Scripts/Site/skrollr.min.js",
                      "~/Scripts/Site/jquery.scrollTo-1.4.3.1-min.js",
                      "~/Scripts/Site/jquery.localscroll-1.2.7-min.js",
                      "~/Scripts/Site/stellar.js",
                      "~/Scripts/Site/jquery.appear.js",
                      "~/Scripts/Site/validate.js",
                      "~/Scripts/Site/main.js",
                      "~/Scripts/jquery.maskedinput.min.js",
                      "~/Scripts/jquery.maskMoney.min.js",
                      "~/Scripts/alertify.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/loginAdmin").Include(
                "~/Scripts/Site/LoginAdmin",
                "~/Scripts/jquery.validate*",
                "~/Scripts/bootstrap.js",
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                        //  "~/Content/animate.css",
                      //  "~/Content/font-awesome.css",
                      //  "~/Content/overwrite.css",
                      "~/Content/bootstrap-theme.css",
                      "~/Content/default.css",
                      "~/Content/isotioe.css",
                      "~/Content/style.css",
                      "~/Content/jquery.fancybox.css"));

            bundles.Add(new StyleBundle("~/Content/loginAdmin").Include(
                "~/Content/LoginAdmin.css",
                "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/admin").Include(
             "~/Content/bootstrap.css",
             "~/Content/AdminLTE.css",
             "~/Content/font-awesome.css",
             "~/Content/google-chart.css"));

            bundles.Add(new StyleBundle("~/Content/alertify").Include(
            "~/Content/alertify/alertify.min.css",
            "~/Content/alertify/themes/default.min.css",
            "~/Content/alertify/themes/semantic.min.css"
        ));
        }
    }
}
