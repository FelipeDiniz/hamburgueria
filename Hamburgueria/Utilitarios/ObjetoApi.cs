﻿using System.Collections.Generic;

namespace Hamburgueria.Controllers
{
    public abstract class ObjetoApi<T> where T : new()
    {
        public static IList<T> Lista(System.Data.DataTable dataTable)
        {
            return PopuladorDeObjeto<T>.Lista(dataTable);
        }

        public static T Popula(System.Data.DataRow row)
        {
            return PopuladorDeObjeto<T>.Popula(row);
        }
    }
}