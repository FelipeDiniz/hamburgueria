﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace Hamburgueria.Controllers
{
    public class PopuladorDeObjeto<Objeto> where Objeto : new()
    {
        public static List<Objeto> Lista(DataTable table)
        {
            var lista = new List<Objeto>();

            foreach (DataRow row in table.Rows)
            {
                var item = new Objeto();

                for (int i = 0; i < row.Table.Columns.Count; i++)
                {
                    var nomePropriedade = row.Table.Columns[i].ColumnName.ToLower();

                    foreach (var prop in typeof(Objeto).GetProperties())
                        if (nomePropriedade.ToLower() == prop.Name.ToLower())
                            SetaValor(item, prop, row[i]);
                }

                lista.Add(item);
            }

            return lista;
        }

        public static Objeto Popula(DataRow row)
        {
            var obj = new Objeto();
            for (int i = 0; i < row.Table.Columns.Count; i++)
            {
                var nomePropriedade = row.Table.Columns[i].ColumnName.ToLower();

                foreach (var prop in obj.GetType().GetProperties())
                {
                    if (nomePropriedade == prop.Name.ToLower())
                    {
                        SetaValor(obj, prop, row[i]);
                    }
                }
            }

            return obj;
        }

        static void SetaValor(Objeto objeto, PropertyInfo property, object valor)
        {
           string tipoDeVariavelDaPropriedade;
            if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                tipoDeVariavelDaPropriedade = property.PropertyType.GetGenericArguments()[0].FullName;
            }else{
                tipoDeVariavelDaPropriedade = property.PropertyType.FullName;
            }
            if (!String.IsNullOrEmpty(valor.ToString()))
            {
                switch (tipoDeVariavelDaPropriedade)
                {
                    case "System.Int32":
                        property.SetValue(objeto, Convert.ToInt32(valor), null);
                        break;
                    case "System.Decimal":
                        property.SetValue(objeto, Math.Round(Convert.ToDecimal(valor), 2), null);
                        break;
                    case "System.String":
                        property.SetValue(objeto, valor.ToString(), null);
                        break;
                    case "System.Char":
                        property.SetValue(objeto, Convert.ToChar(valor), null);
                        break;
                    case "System.Double":
                        property.SetValue(objeto, Convert.ToDouble(valor), null);
                        break;
                    case "System.Boolean":
                        property.SetValue(objeto, Convert.ToBoolean(valor), null);
                        break;
                    case "System.DateTime":
                        property.SetValue(objeto, Convert.ToDateTime(valor), null);
                        break;
                }
            }          
        }
    }
}