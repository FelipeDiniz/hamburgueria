function GerarGrafico(parametros, titulo, divId, tipoDeChart) {
        google.load('visualization', '1', {packages: ['corechart']});
        google.setOnLoadCallback(DesempenhoDetalhado);
        function DesempenhoDetalhado(){

            var dados = google.visualization.arrayToDataTable(
                    parametros
            );

            var opcoes = {
                title: titulo,
                legend: { position: "bottom" }
            };

            switch (tipoDeChart) {

                case 0:
                    var chart = new google.visualization.ColumnChart(document.getElementById(divId));
                    break;
                case 1:
                    var chart = new google.visualization.PieChart(document.getElementById(divId));
                    break;
            }

            chart.draw(dados, opcoes);
        }
    }
