﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Hamburgueria.Filtros
{
    public class SomenteAdminLogado : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            string nivelDoUsuario = DadosTemporarios.RetornaNivelDeAcessoDoUsuario();

            if (nivelDoUsuario != "Administrador")
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Admin",
                    action = "Index"
                }));
            }

        }

    }
}