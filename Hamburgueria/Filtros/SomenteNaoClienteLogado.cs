﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Hamburgueria.Filtros
{
    public class SomenteNaoClienteLogado : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            string nivelDoUsuario = DadosTemporarios.RetornaNivelDeAcessoDoUsuario();
            if (nivelDoUsuario == "Cliente" || string.IsNullOrEmpty(nivelDoUsuario))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Admin",
                    action = "Index"
                }));
            }
        }
    }
}