﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Hamburgueria.Filtros
{
    public class SomenteClienteLogado : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            string nivelDoUsuario = DadosTemporarios.RetornaNivelDeAcessoDoUsuario();

            if (nivelDoUsuario != "Cliente")
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Login",
                    action = "Index"
                }));
            }

        }

    }
}