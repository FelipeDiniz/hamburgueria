﻿using System.Web;
using Hamburgueria.Models;
using System.Collections.Generic;

namespace Hamburgueria
{
    public class DadosTemporarios
    {
        const string CHAVE_NIVEL_USUARIO = "NIVEL_USUARIO";
        const string CHAVE_USUARIO = "USUARIO";
        const string CHAVE_NOME_USUARIO = "NOME_DO_USUARIO";
        const string CHAVE_EMAIL_DO_USUARIO = "EMAIL_DO_USUARIO";
        const string CHAVE_PEDIDOS = "PEDIDOS";

        internal static void ArmazenaNivelDeAcessoDoUsuario(string nivelDeAcesso)
        {
            HttpContext.Current.Session[CHAVE_NIVEL_USUARIO] = nivelDeAcesso;
        }
        
        internal static void ArmazenaNomeDoUsuario(string nomeDoUsuario)
        {
            HttpContext.Current.Session[CHAVE_NOME_USUARIO] = nomeDoUsuario;
        }

        internal static void ArmazenaUsuario(Usuarios usuario)
        {
            HttpContext.Current.Session[CHAVE_USUARIO] = usuario;
        }

        internal static void ArmazenaAdminLogado(Administradores admin)
        {
            HttpContext.Current.Session[CHAVE_USUARIO] = admin;
        }

        internal static void ArmazenaEmailDoUsuario(string emailDoUsuario)
        {
            HttpContext.Current.Session[CHAVE_EMAIL_DO_USUARIO] = emailDoUsuario;
        }

        internal static void ArmazenaProdutosDopedido(ProdutosDosPedidos produtoDoPedido)
        {
            List<ProdutosDosPedidos> produtosDopedido = RetornaProdutosDoPedido() != null ?
                RetornaProdutosDoPedido() : new List<ProdutosDosPedidos>();

            produtosDopedido.Add(produtoDoPedido);
            HttpContext.Current.Session[CHAVE_PEDIDOS] = produtosDopedido;
        }

        internal static void RemoveProdutosDopedido(int id)
        {
            List<ProdutosDosPedidos> produtosDopedido = RetornaProdutosDoPedido();
            produtosDopedido.RemoveAt(id);
            HttpContext.Current.Session[CHAVE_PEDIDOS] = produtosDopedido;
        }

        internal static void RemoveProdutosDopedido()
        {
            HttpContext.Current.Session[CHAVE_PEDIDOS] = new List<ProdutosDosPedidos>();
        }

        public static string RetornaNivelDeAcessoDoUsuario()
        {
            return HttpContext.Current.Session[CHAVE_NIVEL_USUARIO] != null ?
                HttpContext.Current.Session[CHAVE_NIVEL_USUARIO].ToString() : "";
        }

        public static string RetornaNomeDoUsuario()
        {
            return HttpContext.Current.Session[CHAVE_NOME_USUARIO] != null ?
                HttpContext.Current.Session[CHAVE_NOME_USUARIO].ToString() : "";
        }

        internal static Usuarios RetornaUsuario()
        {
            return HttpContext.Current.Session[CHAVE_USUARIO] != null ?
                (Usuarios)HttpContext.Current.Session[CHAVE_USUARIO] : null;
        }

        internal static Administradores RetornaAdminLogado()
        {
            return HttpContext.Current.Session[CHAVE_USUARIO] != null ?
                (Administradores)HttpContext.Current.Session[CHAVE_USUARIO] : null;
        }

        public static string RetornaEmailDoUsuario()
        {
            return HttpContext.Current.Session[CHAVE_EMAIL_DO_USUARIO] != null ?
                HttpContext.Current.Session[CHAVE_EMAIL_DO_USUARIO].ToString() : "";
        }

        internal static List<ProdutosDosPedidos> RetornaProdutosDoPedido()
        {
            return HttpContext.Current.Session[CHAVE_PEDIDOS] != null ?
                (List<ProdutosDosPedidos>)HttpContext.Current.Session[CHAVE_PEDIDOS] : null;
        }

    }
}