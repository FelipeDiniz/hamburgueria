﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hamburgueria.Models
{
    [MetadataType(typeof(UsuariosMetadata))]
    public partial class Usuarios
    {
    }

    public class UsuariosMetadata
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Usuário")]
        public string Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Required]
        [Display(Name = "Nome Completo")]
        public string Nome { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Data de  nascimento")]
        public DateTime DataDeNascimento { get; set; }

        [Required]
        [Display(Name = "Desejo receber emails de promoções")]
        public bool DesejaReceberEmail { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        //testar esse método
        public virtual ICollection<Enderecos> Enderecos()
        {
            return new HamburgueriaEntities().Enderecos.Where(e => e.IdUsuario == Id).ToList();
        }

    }
}