﻿using System.ComponentModel.DataAnnotations;

namespace Hamburgueria.Models
{

    [MetadataType(typeof(FaleConoscoMetadata))]
    public partial class FaleConosco
    {

    }

    public class FaleConoscoMetadata
    {

        [Required(ErrorMessage ="O campo \"Nome\" é obrigatório")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O campo \"Assunto\" é obrigatório")]
        public string Assunto { get; set; }

        [DataType(DataType.EmailAddress,ErrorMessage = "Email em formato incorreto")]
        public string Email { get; set; }
 
        [Required(ErrorMessage = "Parece que você esqueceu de escrever a mensagem")]
        public string Mensagem { get; set; }
        public System.DateTime Data { get; set; }
    }
}
