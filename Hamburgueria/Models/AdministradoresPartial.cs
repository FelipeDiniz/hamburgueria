﻿namespace Hamburgueria.Models
{

    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(AdministradoresMetadata))]
    public partial class Administradores
    {
    }


        public partial class AdministradoresMetadata
    {
        [Required(ErrorMessage ="Preencha o campo com seu usuário")]
        public string Usuario { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Preencha o campo com a sua senha")]
        public string Senha { get; set; }

        public virtual TiposDeUsuarios TiposDeUsuarios { get; set; }
    }
}
