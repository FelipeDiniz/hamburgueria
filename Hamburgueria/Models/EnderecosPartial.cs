﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hamburgueria.Models
{
    [MetadataType(typeof(EnderecosMetadata))]
    public partial class Enderecos
    {
    }

    public class EnderecosMetadata
    {
        [Required]
        [Display(Name = "Bairro")]
        public int IdBairro { get; set; }

        [Required]
        [Display(Name ="Endereço")]
        [DataType(DataType.Text)]
        [MaxLength(255,ErrorMessage ="Digite um endereço com no máximo 255 caracteres. Para "
                                    +"qualquer informação adicional use o campo de complemento")]
        public string Endereco { get; set; }

        public string Complemento { get; set; }
        [Required]
        [Display(Name ="Apelide seu endereço (ex: minha casa)")]
        public string ApelidoDoEndereco { get; set; }
    }
}