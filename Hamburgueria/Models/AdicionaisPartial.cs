﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hamburgueria.Models
{
    [MetadataType(typeof(AdicionaisMetadata))]
    public partial class Adicionais
    {

    }

    public class AdicionaisMetadata
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        [Display(Name ="Preço")]
        public decimal Preco { get; set; }
    }
}