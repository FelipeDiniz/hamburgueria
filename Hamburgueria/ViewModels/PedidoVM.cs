﻿using Hamburgueria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hamburgueria.ViewModels
{
    public class PedidoVM
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Nome { get; set; }
        public int IdProdutoDoPedido { get; set; }
        public string Produto { get; set; }
        public IList<AdicionaisDosProdutosDoPedido> AdicionaisDoProdutoDoPedido { get; set; }
        public decimal ValorTotal { get; set; }
        public string Observacoes { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }

    }
}