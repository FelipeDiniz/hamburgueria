﻿using System.ComponentModel.DataAnnotations;

namespace Hamburgueria.ViewModels
{
    public class FinalizarPedidoVM
    {
        public int IdItem { get; set; }
        public string Item { get; set; }
        public string Observacao { get; set; }

        [Display(Name="Preço")]
        public decimal Preco { get; set; }
    }
}