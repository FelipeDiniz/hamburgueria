﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using Arcnet.MyConvert;
using System.Web.Mvc;
using Hamburgueria.Models;
using Hamburgueria.Filtros;

namespace Hamburgueria.Controllers
{
    public class UsuariosController : AutenticacaoController
    {
        // GET: Usuarios
        [SomenteAdminLogado]
        public ActionResult Index()
        {
            var usuarios = db.Usuarios.Include(u => u.TiposDeUsuarios);
            return View(usuarios.ToList());
        }

        // GET: Usuarios/Details/5
        [SomenteAdminLogado]
        public ActionResult Detalhes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuarios usuarios = db.Usuarios.Find(id);
            if (usuarios == null)
            {
                return HttpNotFound();
            }
            return View(usuarios);
        }

        // GET: Usuarios/Create
        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(string Usuario, string Senha, string Nome, DateTime DataDeNascimento, bool DesejaReceberEmail, string Email, string confirmaSenha)
        {
            //Por algum motivo, o Bind não deu certo e tive que fazer assim... se for arrumar, lembre-se que o id do tipo de 
            //usuário não é passado no formulário como hidden field então tem que setar como 4 aqui no controller
            Usuarios usuario = new Usuarios()
            {
                Usuario = Usuario,
                Senha = Senha,
                Nome = Nome,
                DataDeNascimento = DataDeNascimento,
                DesejaReceberEmail = DesejaReceberEmail,
                Email = Email,
                IdTiposDeUsuarios = 4
            };
            try
            {

                if (usuario.Senha != confirmaSenha)
                {
                    TempData["MensagemErro"] = "As senhas não conferem.";
                    return View(usuario);

                }
                else if (db.Usuarios.Where(u => u.Email == usuario.Email).Count() != 0)
                {
                    TempData["MensagemErro"] = "Email já cadastrado. Tente usar um diferente. Caso já tenha uma conta e não se lembre dos dados, vá em \"Esqueci minha senha\"";
                    return View(usuario);
                }
                else if (db.Usuarios.Where(u => u.Usuario == usuario.Usuario).Count() != 0)
                {
                    TempData["MensagemErro"] = "Usuário já cadastrado. Tente usar um diferente. Caso já tenha uma conta e não se lembre dos dados, vá em \"Esqueci minha senha\"";
                    return View(usuario);
                }
                else if (ModelState.IsValid)
                {
                    usuario.Senha = MyConvert.ToMd5(usuario.Senha);
                    db.Usuarios.Add(usuario);
                    db.SaveChanges();
                    DadosTemporarios.ArmazenaUsuario(usuario);
                    DadosTemporarios.ArmazenaNivelDeAcessoDoUsuario("Cliente");
                    DadosTemporarios.ArmazenaEmailDoUsuario(usuario.Email);
                    return RedirectToAction("Cadastrar", "Enderecos");
                }
                return View(usuario);
            }
            catch
            {
                TempData["MensagemErro"] = "Parece que algo deu errado. Tente mais tarde. Se o problema persistir, ligue para gente ou " +
                    "entre em contato pelo \"Fale conosco\"";
                return RedirectToAction("Index", "Home");
            }


        }

        [HttpGet]
        [SomenteClienteLogado]
        public ActionResult Editar()
        {
            int id = DadosTemporarios.RetornaUsuario().Id;
            Usuarios usuarios = db.Usuarios.Find(id);
            if (usuarios == null)
            {
                return HttpNotFound();
            }

            if (DadosTemporarios.RetornaUsuario().Id == id)
            {
                return View(usuarios);
            }
            else
            {
                TempData["MensagemErro"] = "Não é possível editar dados de outro usuário além do seu.";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [SomenteClienteLogado]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "Id,IdTiposDeUsuarios,Senha,Usuario,Nome,DataDeNascimento,DesejaReceberEmail,Email")] Usuarios usuarios)
        {
            try
            {
                if (usuarios.Usuario != DadosTemporarios.RetornaUsuario().Usuario && db.Usuarios.Where(u => u.Usuario == usuarios.Usuario).FirstOrDefault() != null)
                {
                    TempData["MensagemErro"] = "Não foi possível editar dados do seu usuário pois já existe um usuário com o mesmo nome no sistema.";
                    return RedirectToAction("Index", "Home");
                }

                if (usuarios.Email != DadosTemporarios.RetornaUsuario().Email && db.Usuarios.Where(u => u.Email == usuarios.Email).FirstOrDefault() != null)
                {
                    TempData["MensagemErro"] = "Não foi possível editar dados do seu usuário pois já existe um usuário com o mesmo nome no sistema.";
                    return RedirectToAction("Index", "Home");
                }

                usuarios.IdTiposDeUsuarios = 4;
                if (ModelState.IsValid)
                {
                    db.Entry(usuarios).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["MensagemSucesso"] = "Senha alterada com sucesso";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["MensagemErro"] = "Parece que algo deu errado no envio do formulário";
                    return View("Editar");
                }

            }
            catch (Exception e)
            {
                TempData["MensagemErro"] = "Parece que algo deu errado. Tente novamente mais tarde";
                return View("Editar");
            }
        }

        [HttpPost]
        [SomenteClienteLogado]
        public ActionResult AlterarSenha(string senha, string novaSenha, string repetirSenha)
        {
            try
            {
                Usuarios usuario = DadosTemporarios.RetornaUsuario();
                if (usuario.Senha == MyConvert.ToMd5(senha))
                {
                    if (novaSenha != repetirSenha)
                    {
                        TempData["MensagemErro"] = "As senhas não conferem. Tente novamente";
                        return View("Editar");
                    }
                    usuario.Senha = MyConvert.ToMd5(novaSenha);
                    db.Usuarios.Update(usuario);
                    db.SaveChanges();
                    TempData["MensagemSucesso"] = "Senha alterada com sucesso";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["MensagemErro"] = "Senha incorreta. Tente novamente";
                    return View("Editar");
                }
            }
            catch
            {
                TempData["MensagemErro"] = "Parece que algo deu errado. Tente novamente mais tarde";
                return View("Editar");
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
