﻿using Hamburgueria.Models;
using System.Web.Mvc;

namespace Hamburgueria.Controllers
{
    public class AutenticacaoController : Controller
    {
        public HamburgueriaEntities db = new HamburgueriaEntities();

        // GET: Autenticacao
        public AutenticacaoController()
        {
            db = new HamburgueriaEntities();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return this.RedirectToAction("Index", "Home");
        }

    }
}