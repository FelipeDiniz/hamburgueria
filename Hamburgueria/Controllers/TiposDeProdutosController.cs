﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hamburgueria.Models;

namespace Hamburgueria.Controllers
{
    public class TiposDeProdutosController : AutenticacaoController

    {
        // GET: TiposDeProdutos
        public ActionResult Index()
        {
            return View(db.TiposDeProdutos.ToList());
        }

        // GET: TiposDeProdutos/Cadastrar
        public ActionResult Criar()
        {
            return View();
        }

        // POST: TiposDeProdutos/Cadastrar
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Criar([Bind(Include = "Id,Nome")] TiposDeProdutos tiposDeProdutos)
        {
            if (ModelState.IsValid)
            {
                db.TiposDeProdutos.Add(tiposDeProdutos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tiposDeProdutos);
        }

        // GET: TiposDeProdutos/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposDeProdutos tiposDeProdutos = db.TiposDeProdutos.Find(id);
            if (tiposDeProdutos == null)
            {
                return HttpNotFound();
            }
            return View(tiposDeProdutos);
        }

        // POST: TiposDeProdutos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "Id,Nome")] TiposDeProdutos tiposDeProdutos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tiposDeProdutos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tiposDeProdutos);
        }

        // GET: TiposDeProdutos/Delete/5
        public ActionResult Deletar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposDeProdutos tiposDeProdutos = db.TiposDeProdutos.Find(id);
            if (tiposDeProdutos == null)
            {
                return HttpNotFound();
            }
            return View(tiposDeProdutos);
        }

        // POST: TiposDeProdutos/Delete/5
        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TiposDeProdutos tiposDeProdutos = db.TiposDeProdutos.Find(id);
            db.TiposDeProdutos.Remove(tiposDeProdutos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
