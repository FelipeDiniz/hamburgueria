﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hamburgueria.Models;
using Hamburgueria.Filtros;

namespace Hamburgueria.Controllers
{
    [SomenteClienteLogado]
    public class EnderecosController : AutenticacaoController
    {
        // GET: Enderecos
        public ActionResult Index()
        {
            int idUsuario = DadosTemporarios.RetornaUsuario().Id;
            var enderecos = db.Enderecos.Where(e => e.IdUsuario == idUsuario);
            return View(enderecos.ToList());
        }

        // GET: Enderecos/Create
        public ActionResult Cadastrar()
        {
            ViewBag.IdBairro = new SelectList(db.Bairros, "Id", "Bairro");
            return View();
        }

        // POST: Enderecos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar([Bind(Include = "Id,IdBairro,Endereco,Complemento,ApelidoDoEndereco")] Enderecos enderecos)
        {
            enderecos.IdUsuario = DadosTemporarios.RetornaUsuario().Id;
            if (ModelState.IsValid)
            {
                db.Enderecos.Add(enderecos);
                db.SaveChanges();
                TempData["MensagemSucesso"] = "Novo endereço cadastrado com sucesso!";
                return RedirectToAction("Index", "Home");
            }

            ViewBag.IdBairro = new SelectList(db.Bairros, "Id", "Bairro", enderecos.IdBairro);
            return View(enderecos);
        }

        // GET: Enderecos/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if(db.Enderecos.FirstOrDefault(e => e.Id == id).IdUsuario != DadosTemporarios.RetornaUsuario().Id)
            {
                TempData["MensagemErro"] = "Não é possível mudar endereços de outros usuários";
                return RedirectToAction("Index");
            }
            Enderecos enderecos = db.Enderecos.Find(id);
            if (enderecos == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdBairro = new SelectList(db.Bairros, "Id", "Bairro", enderecos.IdBairro);
            return View(enderecos);
        }

        // POST: Enderecos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "Id,IdBairro,Endereco,Complemento,ApelidoDoEndereco")] Enderecos enderecos)
        {
            enderecos.IdUsuario = DadosTemporarios.RetornaUsuario().Id;
            if (ModelState.IsValid)
            {
                db.Entry(enderecos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdBairro = new SelectList(db.Bairros, "Id", "Bairro", enderecos.IdBairro);
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "Id", "Usuario", enderecos.IdUsuario);
            return View(enderecos);
        }

        // GET: Enderecos/Delete/5
        public ActionResult Deletar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (db.Enderecos.FirstOrDefault(e => e.Id == id).IdUsuario != DadosTemporarios.RetornaUsuario().Id)
            {
                TempData["MensagemErro"] = "Não é possível deletar endereços de outros usuários";
                return RedirectToAction("Index");
            }
            Enderecos enderecos = db.Enderecos.Find(id);
            if (enderecos == null)
            {
                return HttpNotFound();
            }
            return View(enderecos);
        }

        // POST: Enderecos/Delete/5
        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (db.Enderecos.FirstOrDefault(e => e.Id == id).IdUsuario != DadosTemporarios.RetornaUsuario().Id)
            {
                TempData["MensagemErro"] = "Não é possível deletar endereços de outros usuários";
                return RedirectToAction("Index");
            }
            Enderecos enderecos = db.Enderecos.Find(id);
            db.Enderecos.Remove(enderecos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
