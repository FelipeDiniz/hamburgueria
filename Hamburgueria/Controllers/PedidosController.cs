﻿using Hamburgueria.Filtros;
using Hamburgueria.Models;
using Hamburgueria.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hamburgueria.Controllers
{
    [SomenteClienteLogado]
    public class PedidosController : AutenticacaoController
    {
        // GET: Pedido
        public ActionResult Index()
        {
            List<Produtos> produtos = db.Produtos.OrderBy(x => x.IdTipoDoProduto).ToList();
            return View(produtos);
        }

        [HttpGet]
        public ActionResult Adicionar(int id)
        {
            Produtos produto = db.Produtos.Where(x => x.Id == id).Single();
            List<Adicionais> adicionais = produto.IdTipoDoProduto == 1 ? db.Adicionais.ToList() : new List<Adicionais>(); //IdTipoDoProduto = 1: hamburuguer
            Tuple<Produtos, List<Adicionais>> tuple = new Tuple<Produtos, List<Adicionais>>(produto, adicionais);
            return View(tuple);
        }

        public ActionResult Adicionar(List<bool> adicionais, List<int> idAdicionais, string observacao, int idProduto)
        {
            List<AdicionaisDosProdutosDoPedido> adicionaisDosPedidos = new List<AdicionaisDosProdutosDoPedido>();
            if (adicionais != null)
                for (int i = 0; i < adicionais.Count; i++)
                {
                    if (adicionais[i])
                    {
                        //Não entendi por que mas quando o checkbox é true ele envia um false do mesmo ítem...
                        //No HTML ele aparece como campo hidden... resolvi em uma linha mas é mais bonito,
                        //futuramentem editar o razor na view.
                        adicionais.Remove(adicionais[i + 1]);
                        AdicionaisDosProdutosDoPedido adicionaisDoProduto = new AdicionaisDosProdutosDoPedido
                        {
                            IdAdicional = idAdicionais[i]
                        };
                        adicionaisDoProduto.Adicionais = db.Adicionais.Where(x => x.Id == adicionaisDoProduto.IdAdicional).Single();
                        adicionaisDosPedidos.Add(adicionaisDoProduto);
                    }
                }

            ProdutosDosPedidos produtoDoPedido = new ProdutosDosPedidos
            {
                IdProduto = idProduto,
                Observacoes = observacao,
                AdicionaisDosProdutosDoPedido = adicionaisDosPedidos // as ICollection<AdicionaisDosProdutosDoPedido>
            };
            CalculaValorDoProduto(produtoDoPedido);
            produtoDoPedido.Produtos = db.Produtos.Where(x => x.Id == produtoDoPedido.IdProduto).Single();
            DadosTemporarios.ArmazenaProdutosDopedido(produtoDoPedido);

            List<Produtos> produtos = db.Produtos.OrderBy(x => x.IdTipoDoProduto).ToList();
            return View("Index", produtos);
        }

        [HttpGet]
        public ActionResult Finalizar()
        {
            try
            {

                if (DadosTemporarios.RetornaProdutosDoPedido() == null || DadosTemporarios.RetornaProdutosDoPedido().Count < 1)
                {
                    TempData["MensagemErro"] = "Parece que ainda não há nenhum produto selcionado para o pedido. "
                        + "Selecione os produtos do pedido antes de finalizar";
                    return RedirectToAction("index", "Pedidos");
                }

                List<FinalizarPedidoVM> finalizarPedidoVM = new List<FinalizarPedidoVM>();
                int i = 0;
                foreach (ProdutosDosPedidos produtoDoPedido in DadosTemporarios.RetornaProdutosDoPedido())
                {
                    string adicionaisConcatenados = string.Empty;
                    if (produtoDoPedido.AdicionaisDosProdutosDoPedido != null)
                        for (int count = 0; count < produtoDoPedido.AdicionaisDosProdutosDoPedido.Count; count++)
                        {
                            List<AdicionaisDosProdutosDoPedido> adicionais = produtoDoPedido.AdicionaisDosProdutosDoPedido.ToList();
                            if (count + 1 == produtoDoPedido.AdicionaisDosProdutosDoPedido.Count)
                            {
                                adicionaisConcatenados = string.Concat(adicionaisConcatenados, adicionais[count].Adicionais.Nome);
                                break;
                            }
                            else
                            {
                                adicionaisConcatenados = string.Concat(adicionaisConcatenados, adicionais[count].Adicionais.Nome, " + ");
                            }
                        }

                    finalizarPedidoVM.Add(new FinalizarPedidoVM
                    {
                        IdItem = i,
                        Item = adicionaisConcatenados == string.Empty ? produtoDoPedido.Produtos.Nome : string.Concat(produtoDoPedido.Produtos.Nome, " + ", adicionaisConcatenados),
                        Preco = (decimal)produtoDoPedido.Valor,
                        Observacao = produtoDoPedido.Observacoes

                    });
                }
                int idUsuario = DadosTemporarios.RetornaUsuario().Id;
                ViewBag.idEndereco = new SelectList(db.Enderecos.Where(x => x.IdUsuario == idUsuario).ToList(), "Id", "ApelidoDoEndereco");
                ViewBag.idFormaDePagamento = new SelectList(db.FormasDePagamento.ToList(), "Id", "FormaDePagamento");
                return View(finalizarPedidoVM);
            }catch
            {
                TempData["MensagemErro"] = "Parece que algo deu errado. Tente mais tarde. Se o problema persistir, ligue para gente ou " +
                    "entre em contato pelo \"Fale conosco\"";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Finalizar(int idFormaDePagamento, decimal? trocoPara, int idEndereco)
        {
            try
            {
                using (var _context = new HamburgueriaEntities())
                {
                    Pedidos pedido = new Pedidos()
                    {
                        IdUsuario = DadosTemporarios.RetornaUsuario().Id,
                        Data = DateTime.Now,
                        ValorTotal = (decimal)DadosTemporarios.RetornaProdutosDoPedido().Sum(x => x.Valor),
                        IdFormaDePagamento = idFormaDePagamento,
                        TrocoPara = trocoPara == null ? 0 : trocoPara,
                        IdEndereco = idEndereco,
                        IdStatusPedido = 1
                    };
                    List<ProdutosDosPedidos> produtosDoPedido = DadosTemporarios.RetornaProdutosDoPedido();
                    produtosDoPedido.ForEach(x => x.Produtos = null);
                    for (int i = 0; i < produtosDoPedido.Count; i++)
                    {
                        produtosDoPedido[i].AdicionaisDosProdutosDoPedido.ToList().ForEach(x => x.Adicionais = null);
                        pedido.ProdutosDosPedidos.Add(produtosDoPedido[i]);
                    }
                    _context.Pedidos.Add(pedido);
                    _context.SaveChanges();

                    DadosTemporarios.RemoveProdutosDopedido();
                    TempData["MensagemSucesso"] = "Pronto! Agora é só esperar tranquilo até seu pedido chegar";
                    return RedirectToAction("index", "Home");
                }
            }
            catch
            {
                TempData["MensagemErro"] = "Parece que algo deu errado. Tente mais tarde. Se o problema persistir, ligue para gente ou " +
                    "entre em contato pelo \"Fale conosco\"";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        public ActionResult Remover(int id)
        {

            DadosTemporarios.RemoveProdutosDopedido(id);
            return RedirectToAction("Finalizar");
        }

        public void CalculaValorDoProduto(ProdutosDosPedidos produtoDoPedido)
        {
            decimal valorDosAdicionais = 0;

            if (produtoDoPedido.AdicionaisDosProdutosDoPedido.ToList() != null)
                foreach (var adicionais in produtoDoPedido.AdicionaisDosProdutosDoPedido.ToList())
                    valorDosAdicionais += db.Adicionais.Where(x => x.Id == adicionais.IdAdicional).Single().Preco;


            produtoDoPedido.Valor = db.Produtos.Where(x => x.Id == produtoDoPedido.IdProduto).Single().Preco + valorDosAdicionais;
        }

    }
}