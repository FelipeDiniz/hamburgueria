﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hamburgueria.Models;
using Hamburgueria.Filtros;

namespace Hamburgueria.Controllers
{
    [SomenteAdminLogado]
    public class BairrosController : AutenticacaoController
    {
        // GET: Bairros
        public ActionResult Index()
        {
            return View(db.Bairros.ToList());
        }

        // GET: Bairros/Create
        public ActionResult Cadastrar()
        {
            return View();
        }

        // POST: Bairros/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar([Bind(Include = "Id,Bairro")] Bairros bairros)
        {
            if (ModelState.IsValid)
            {
                db.Bairros.Add(bairros);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bairros);
        }

        // GET: Bairros/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bairros bairros = db.Bairros.Find(id);
            if (bairros == null)
            {
                return HttpNotFound();
            }
            return View(bairros);
        }

        // POST: Bairros/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "Id,Bairro")] Bairros bairros)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bairros).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bairros);
        }

        // GET: Bairros/Delete/5
        public ActionResult Deletar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bairros bairros = db.Bairros.Find(id);
            if (bairros == null)
            {
                return HttpNotFound();
            }
            return View(bairros);
        }

        // POST: Bairros/Delete/5
        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bairros bairros = db.Bairros.Find(id);
            db.Bairros.Remove(bairros);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
