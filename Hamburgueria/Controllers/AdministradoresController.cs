﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hamburgueria.Models;

namespace Hamburgueria.Controllers
{
    public class AdministradoresController : AutenticacaoController
    {

        // GET: Administradores
        public ActionResult Index()
        {
            var administradores = db.Administradores.Include(a => a.TiposDeUsuarios);
            return View(administradores.ToList());
        }

        // GET: Administradores/Details/5
        public ActionResult Detalhes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administradores administradores = db.Administradores.Find(id);
            if (administradores == null)
            {
                return HttpNotFound();
            }
            return View(administradores);
        }

        // GET: Administradores/Cadastrar
        public ActionResult Criar()
        {
            ViewBag.IdTipoDeUsuarios = new SelectList(db.TiposDeUsuarios, "Id", "TipoDeUsuario");
            return View();
        }

        // POST: Administradores/Cadastrar
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Criar([Bind(Include = "Id,IdTipoDeUsuarios,Usuario,Senha")] Administradores administradores)
        {
            if (ModelState.IsValid)
            {
                db.Administradores.Add(administradores);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdTipoDeUsuarios = new SelectList(db.TiposDeUsuarios, "Id", "TipoDeUsuario", administradores.IdTipoDeUsuarios);
            return View(administradores);
        }

        // GET: Administradores/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administradores administradores = db.Administradores.Find(id);
            if (administradores == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdTipoDeUsuarios = new SelectList(db.TiposDeUsuarios, "Id", "TipoDeUsuario", administradores.IdTipoDeUsuarios);
            return View(administradores);
        }

        // POST: Administradores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "Id,IdTipoDeUsuarios,Usuario,Senha")] Administradores administradores)
        {
            if (ModelState.IsValid)
            {
                db.Entry(administradores).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdTipoDeUsuarios = new SelectList(db.TiposDeUsuarios, "Id", "TipoDeUsuario", administradores.IdTipoDeUsuarios);
            return View(administradores);
        }

        // GET: Administradores/Delete/5
        public ActionResult Deletar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administradores administradores = db.Administradores.Find(id);
            if (administradores == null)
            {
                return HttpNotFound();
            }
            return View(administradores);
        }

        // POST: Administradores/Delete/5
        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Administradores administradores = db.Administradores.Find(id);
            db.Administradores.Remove(administradores);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
