﻿using Arcnet.MyConvert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hamburgueria.Controllers
{
    public class LoginController : AutenticacaoController
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string usuario, string senha)
        {
            senha = MyConvert.ToMd5(senha);
            try
            {
                var usuarioLogado = db.Usuarios.Where(x => x.Usuario == usuario).SingleOrDefault();
                if(usuarioLogado == null)
                {
                    TempData["MensagemErro"] = "Usuário não encontrado";
                    return RedirectToAction("Index", "Login");
                }
                else if (usuarioLogado.Senha != senha)
                {
                    TempData["MensagemErro"] = "Senha incorreta. Tente novamente";
                    return RedirectToAction("Index", "Login");
                }
                else
                {
                    DadosTemporarios.ArmazenaUsuario(usuarioLogado);
                    DadosTemporarios.ArmazenaNivelDeAcessoDoUsuario("Cliente");
                    DadosTemporarios.ArmazenaEmailDoUsuario(usuarioLogado.Email);
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                TempData["MensagemErro"] = "Falha ao tentar logar! Verifique seu usuario e senha";
                return RedirectToAction("Index", "Conta");
            }
        }
    }
}