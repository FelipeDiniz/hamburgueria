﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hamburgueria.Models;

namespace Hamburgueria.Controllers
{
    public class StatusPedidosController : AutenticacaoController
    {

        // GET: StatusPedidos
        public ActionResult Index()
        {
            return View(db.StatusPedidos.ToList());
        }

        // GET: StatusPedidos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StatusPedidos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StatusPedido")] StatusPedidos statusPedidos)
        {
            if (ModelState.IsValid)
            {
                db.StatusPedidos.Add(statusPedidos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(statusPedidos);
        }

        // GET: StatusPedidos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StatusPedidos statusPedidos = db.StatusPedidos.Find(id);
            if (statusPedidos == null)
            {
                return HttpNotFound();
            }
            return View(statusPedidos);
        }

        // POST: StatusPedidos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StatusPedido")] StatusPedidos statusPedidos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(statusPedidos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(statusPedidos);
        }

        // GET: StatusPedidos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else if (id <= 4)
            {
                TempData["MensagemErro"] = "Não é possível deletar as informações dos status iniciais do sistema.";
                return RedirectToAction("Index", "StatusPedido");
            }
            try
            {
                StatusPedidos statusPedidos = db.StatusPedidos.Find(id);
                if (statusPedidos == null)
                {
                    return HttpNotFound();
                }
                return View(statusPedidos);
            }
            catch
            {
                TempData["MensagemErro"] = "Não é possível deletar status que estão sendo utilizados pelo sistema";
                return RedirectToAction("Index", "StatusPedido");
            }
        }

        // POST: StatusPedidos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StatusPedidos statusPedidos = db.StatusPedidos.Find(id);
            db.StatusPedidos.Remove(statusPedidos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
