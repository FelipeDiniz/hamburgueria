﻿using Arcnet.MyConvert;
using Hamburgueria.Filtros;
using Hamburgueria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HamburgueriaDAL;
using System.Configuration;
using System.Data;
using Hamburgueria.ViewModels;

namespace Hamburgueria.Controllers
{
    public class AdminController : AutenticacaoController
    {
        // GET: Admin
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string usuario, string senha)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    senha = MyConvert.ToMd5(senha);
                    var usuarioLogin = db.Administradores.Where(x => x.Usuario == usuario).SingleOrDefault();
                    if (usuarioLogin == null)
                    {
                        TempData["MensagemErro"] = "Usuário não encontrado. Tente novamente";
                        return View();
                    }

                    usuarioLogin = db.Administradores.Where(x => x.Usuario == usuario && x.Senha == senha).SingleOrDefault();

                    if (usuarioLogin == null)
                    {
                        TempData["MensagemErro"] = "Senha incorreta para o usuário " + usuario;
                        return View();
                    }
                    else
                    {
                        DadosTemporarios.ArmazenaNivelDeAcessoDoUsuario(usuarioLogin.TiposDeUsuarios.TipoDeUsuario);
                        DadosTemporarios.ArmazenaAdminLogado(usuarioLogin);
                    }
                }
                catch (Exception e)
                {
                    TempData["MensagemErro"] = "Parece que algo deu errado. Se o erro persistir, entre em contato conosco.<p>Código do erro:" + e.HResult + "</p>";
                    return View();
                }
            }
            return RedirectToAction("Pedidos");
        }

        [SomenteNaoClienteLogado]
        public ActionResult Pedidos()
        {
           List<Pedidos> pedidos = db.Pedidos.Where(x => x.IdStatusPedido != 2).ToList();
            return View(pedidos);
        }

        [SomenteNaoClienteLogado]
        public ActionResult EditarPedido(int id)
        {
            try
            {
                Pedidos pedido = db.Pedidos.Where(x => x.Id == id).Single();
                int idUsuario = pedido.Usuarios.Id;
                ViewBag.idEndereco = new SelectList(db.Enderecos.Where(x => x.IdUsuario == idUsuario).ToList(), "Id", "ApelidoDoEndereco");
                ViewBag.idFormaDePagamento = new SelectList(db.FormasDePagamento.ToList(), "Id", "FormaDePagamento");
                return View(pedido);
            }
            catch(Exception e)
            {
                TempData["MensagemErro"] = "Erro ao tentar editar o pedido. Código do erro:" + e.HResult;
                List<Pedidos> pedidos = db.Pedidos.Where(x => x.IdStatusPedido != 2).ToList();
                return View(pedidos);
            }
        }

        
        [SomenteNaoClienteLogado]
        public ActionResult DetalhesDoPedido(int id)
        {
            //try
            //{
                DAOHamburgueria dao = new DAOHamburgueria(ConfigurationManager.ConnectionStrings["HamburgueriaSQLEntities"].ConnectionString);
                DataTable tabelaPedidos = new DataTable();
                tabelaPedidos = dao.Pedido(id);
                List<PedidoVM> pedidos = ObjetoApi<PedidoVM>.Lista(tabelaPedidos).ToList();
                foreach (PedidoVM pedido in pedidos)
                {
                    pedido.AdicionaisDoProdutoDoPedido = db.AdicionaisDosProdutosDoPedido.Where(x => x.IdProdutoDoPedido == pedido.IdProdutoDoPedido).ToList();
                }

                    return View(pedidos);
            //}
            //catch (Exception e)
            //{
            //    TempData["MensagemErro"] = "Erro ao tentar ver os detalhes do pedido. Código do erro:" + e.HResult;
            //    List<Pedidos> pedidos = db.Pedidos.Where(x => x.IdStatusPedido != 2).ToList();
            //    return View(pedidos);
            //}
        }
        [SomenteNaoClienteLogado]
        public ActionResult Graficos()
        {
            return View();
        }
    }
}