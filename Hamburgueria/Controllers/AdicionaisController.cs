﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hamburgueria.Models;

namespace Hamburgueria.Controllers
{
    public class AdicionaisController : AutenticacaoController
    { 
        // GET: Adicionais
    public ActionResult Index()
        {
            return View(db.Adicionais.ToList());
        }

        // GET: Adicionais/Details/5
        public ActionResult Detalhes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adicionais adicionais = db.Adicionais.Find(id);
            if (adicionais == null)
            {
                return HttpNotFound();
            }
            return View(adicionais);
        }

        // GET: Adicionais/Cadastrar
        public ActionResult Criar()
        {
            return View();
        }

        // POST: Adicionais/Cadastrar
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Criar([Bind(Include = "Id,Nome,Preco")] Adicionais adicionais)
        {
            if (ModelState.IsValid)
            {
                db.Adicionais.Add(adicionais);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(adicionais);
        }

        // GET: Adicionais/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adicionais adicionais = db.Adicionais.Find(id);
            if (adicionais == null)
            {
                return HttpNotFound();
            }
            return View(adicionais);
        }

        // POST: Adicionais/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "Id,Nome,Preco")] Adicionais adicionais)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adicionais).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(adicionais);
        }

        // GET: Adicionais/Delete/5
        public ActionResult Deletar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adicionais adicionais = db.Adicionais.Find(id);
            if (adicionais == null)
            {
                return HttpNotFound();
            }
            return View(adicionais);
        }

        // POST: Adicionais/Delete/5
        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Adicionais adicionais = db.Adicionais.Find(id);
            db.Adicionais.Remove(adicionais);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
