﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Hamburgueria.Models;
using System.Web;
using System.IO;
using System;
using System.Data;
using Hamburgueria.Filtros;

namespace Hamburgueria.Controllers
{
    [SomenteAdminLogado]
    public class ProdutosController : AutenticacaoController
    {
        // GET: Produtos
        public ActionResult Index()
        {
            var produtos = db.Produtos.Include(p => p.TiposDeProdutos);
            return View(produtos.ToList());
        }

        // GET: Produtos/Details/5
        public ActionResult Detalhes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produtos produtos = db.Produtos.Find(id);
            if (produtos == null)
            {
                return HttpNotFound();
            }
            return View(produtos);
        }

        // GET: Produtos/Cadastrar
        public ActionResult Criar()
        {
            ViewBag.IdTipoDoProduto = new SelectList(db.TiposDeProdutos, "Id", "Nome");
            Produtos model = new Produtos();
            return View(model);
        }

        // POST: Produtos/Cadastrar
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Criar([Bind(Include = "IdTipoDoProduto,Nome,Preco,Descricao")] Produtos produtos, HttpPostedFileBase image)
        {
            try
            {
                if (image.FileName == string.Empty & image.ContentLength == 0)
                {
                    throw new FileNotFoundException("Imagem não encontrada no caminho especifiado. Informe um caminho válido");
                }

                string tipoDoProduto = db.TiposDeProdutos.Where(x => x.Id == produtos.IdTipoDoProduto).Select(x => x.Nome).FirstOrDefault();
                produtos.Imagem = string.Concat(produtos.Nome, " - ", tipoDoProduto, Path.GetExtension(image.FileName));
                string caminho = Path.Combine(Server.MapPath("~/Img/produtos/") + produtos.Imagem);

                if (System.IO.File.Exists(caminho))
                {
                    throw new ConstraintException(@"Já existe um produto do tipo <i>" + tipoDoProduto + "</i> cadastrado com o nome " + produtos.Nome +
                                ".<p>Delete o produto anterior ou clique em alterar</p>");
                }
                if (ModelState.IsValid)
                {
                    image.SaveAs(caminho);
                    db.Produtos.Add(produtos);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (FileNotFoundException e)
            {
                TempData["MensagemErro"] = e.Message;
            }
            catch (ConstraintException e)
            {
                TempData["MensagemErro"] = e.Message;
            }
            catch (Exception e)
            {

                string tipoDoProduto = db.TiposDeProdutos.Where(x => x.Id == produtos.IdTipoDoProduto).Select(x => x.Nome).FirstOrDefault();
                produtos.Imagem = string.Concat(produtos.Nome, " - ", tipoDoProduto, Path.GetExtension(image.FileName));
                string caminho = Path.Combine(Server.MapPath("~/Img/produtos/") + produtos.Imagem);

                if (System.IO.File.Exists(caminho))
                    System.IO.File.Delete(caminho);

                TempData["MensagemErro"] = "Erro ao tentar criar novo produto. Código do erro:" + e.HResult;
            }

            ViewBag.IdTipoDoProduto = new SelectList(db.TiposDeProdutos, "Id", "Nome", produtos.IdTipoDoProduto);
            return View(produtos);
        }

        // GET: Produtos/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produtos produtos = db.Produtos.Find(id);
            if (produtos == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdTipoDoProduto = new SelectList(db.TiposDeProdutos, "Id", "Nome", produtos.IdTipoDoProduto);
            return View(produtos);
        }

        // POST: Produtos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "Id,IdTipoDoProduto,Nome,Preco,Descricao")] Produtos produtos, HttpPostedFileBase image)
        {
            try
            {
                if (image.FileName == string.Empty & image.ContentLength == 0)
                {
                    throw new FileNotFoundException("Imagem não encontrada no caminho especifiado. Informe um caminho válido");
                }

                string tipoDoProduto = db.TiposDeProdutos.Where(x => x.Id == produtos.IdTipoDoProduto).Select(x => x.Nome).FirstOrDefault();
                produtos.Imagem = string.Concat(produtos.Nome, " - ", tipoDoProduto, Path.GetExtension(image.FileName));
                string caminho = Path.Combine(Server.MapPath("~/Img/produtos/") + produtos.Imagem);

                if (System.IO.File.Exists(caminho))
                {
                    if (System.IO.File.Exists(caminho))
                        System.IO.File.Delete(caminho);
                }
                if (ModelState.IsValid)
                {
                    image.SaveAs(caminho);
                    db.Entry(produtos).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (FileNotFoundException e)
            {
                TempData["MensagemErro"] = e.Message;
            }
            catch (ConstraintException e)
            {
                TempData["MensagemErro"] = e.Message;
            }
            catch (Exception e)
            {

                string tipoDoProduto = db.TiposDeProdutos.Where(x => x.Id == produtos.IdTipoDoProduto).Select(x => x.Nome).FirstOrDefault();
                produtos.Imagem = string.Concat(produtos.Nome, " - ", tipoDoProduto, Path.GetExtension(image.FileName));
                string caminho = Path.Combine(Server.MapPath("~/Img/produtos/") + produtos.Imagem);

                if (System.IO.File.Exists(caminho))
                    System.IO.File.Delete(caminho);

                TempData["MensagemErro"] = "Erro ao tentar criar novo produto. Código do erro:" + e.HResult;
            }

            ViewBag.IdTipoDoProduto = new SelectList(db.TiposDeProdutos, "Id", "Nome", produtos.IdTipoDoProduto);
            return View(produtos);
        }

        // GET: Produtos/Delete/5
        public ActionResult Deletar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produtos produtos = db.Produtos.Find(id);
            if (produtos == null)
            {
                return HttpNotFound();
            }
            return View(produtos);
        }

        // POST: Produtos/Delete/5
        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Produtos produtos = db.Produtos.Find(id);
            db.Produtos.Remove(produtos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
