﻿using Hamburgueria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hamburgueria.Controllers
{
    public class HomeController : AutenticacaoController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string Nome, string Email, string Assunto, string Mensagem, DateTime Data)
        {
            if (ModelState.IsValid)
            {
                FaleConosco mensagem = new FaleConosco();
                try
                {
                    mensagem.Nome = Nome;
                    mensagem.Email = Email;
                    mensagem.Assunto = Assunto;
                    mensagem.Mensagem = Mensagem;
                    mensagem.Data = Data;
                    db.FaleConosco.Add(mensagem);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    TempData["MensagemErro"] = "Parece que algo deu errado. Se o erro persistir, entre em contato conosco.<p>Código do erro:" + e.HResult +"</p>";
                    return View();
                }
            }
            return View();
        }
    }
}