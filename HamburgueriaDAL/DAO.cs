﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HamburgueriaDAL
{
    public class DAO
    {

        private string conexao;
        public DAO(string stringConnection)
        {

            conexao = stringConnection;

        }

        private SqlConnection AbreConexao()

        {

            SqlConnection cn = new SqlConnection(conexao);

            cn.Open();

            return cn;

        }

        public void FechaConexao(SqlConnection cn)

        {

            if (cn.State == ConnectionState.Open)

                cn.Close();

        }

        public async Task<int> ExecutaComandoAsync(string strQuery)

        {

            SqlConnection cn = new SqlConnection();

            try

            {

                cn = new SqlConnection(conexao);

                await cn.OpenAsync();

                SqlCommand cmd = new SqlCommand();

                cmd.CommandTimeout = 180;

                cmd.CommandText = strQuery.ToString();

                cmd.CommandType = CommandType.Text;

                cmd.Connection = cn;
                return await cmd.ExecuteNonQueryAsync();

            }

            catch (Exception ex)

            {
                throw ex;
            }

            finally

            {
                FechaConexao(cn);
            }

        }


        /// <summary>

        /// Executa procedure e retorna o ID do registro gerado.

        /// </summary>

        /// <param name="cmd">SqlCommand com todos os parâmetros que satisfazem a procedure.</param>

        /// <returns>Id do registro que foi inserido no banco de dados.</returns>

        public async Task<int> ExecutaProcedureAsync(SqlCommand cmd, string nomeParametroRetornoProcedure)

        {

            SqlConnection cn = new SqlConnection();

            try

            {

                cn = new SqlConnection(conexao);

                await cn.OpenAsync();



                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 180;

                cmd.Connection = cn;
                await cmd.ExecuteNonQueryAsync();

                int id = 0;

                Int32.TryParse(cmd.Parameters[nomeParametroRetornoProcedure].Value.ToString(), out id);
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                FechaConexao(cn);
            }

        }

        public async Task<int> ExecutaProcedureAsync(SqlCommand cmd)

        {

            SqlConnection cn = new SqlConnection();

            try

            {

                cn = new SqlConnection(conexao);

                await cn.OpenAsync();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 180;

                cmd.Connection = cn;
                return await cmd.ExecuteNonQueryAsync();

            }

            catch (Exception ex)

            {

                throw ex;

            }

            finally

            {

                FechaConexao(cn);

            }

        }

        public int ExecutaComando(string strQuery)

        {

            SqlConnection cn = new SqlConnection();

            try

            {

                cn = AbreConexao();

                SqlCommand cmd = new SqlCommand();

                cmd.CommandTimeout = 180;

                cmd.CommandText = strQuery.ToString();

                cmd.CommandType = CommandType.Text;

                cmd.Connection = cn;

                return cmd.ExecuteNonQuery();

            }

            catch (Exception ex)

            {

                throw ex;

            }

            finally

            {

                FechaConexao(cn);

            }

        }

        public int ExecutaComandoComTransacao(string strQuery)

        {

            SqlConnection cn = new SqlConnection();

            cn = AbreConexao();



            SqlTransaction tran = cn.BeginTransaction();



            try

            {

                SqlCommand cmd = new SqlCommand();

                cmd.CommandTimeout = 180;

                cmd.CommandText = strQuery.ToString();

                cmd.CommandType = CommandType.Text;

                cmd.Connection = cn;

                cmd.Transaction = tran;

                var retorno = cmd.ExecuteNonQuery();

                tran.Commit();
                return retorno;

            }

            catch (Exception ex)

            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                FechaConexao(cn);
            }

        }

        public dynamic ExecutaEscalar(string strQuery)

        {

            SqlConnection cn = new SqlConnection();

            try

            {

                cn = AbreConexao();

                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = strQuery.ToString();

                cmd.CommandType = CommandType.Text;

                cmd.Connection = cn;

                return cmd.ExecuteScalar();

            }

            catch (Exception ex)

            {

                throw ex;

            }

            finally

            {

                FechaConexao(cn);

            }

        }

        public DataTable RetornaDataTable(string strQuery)

        {

            SqlConnection cn = new SqlConnection();

            try

            {

                cn = AbreConexao();

                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = strQuery.ToString();

                cmd.CommandType = CommandType.Text;

                cmd.CommandTimeout = 1080;

                cmd.Connection = cn;



                var dataAdapter = new SqlDataAdapter();

                var table = new DataTable();

                dataAdapter.SelectCommand = cmd;

                dataAdapter.Fill(table);

                return table;

            }

            catch (Exception ex)

            {

                throw ex;

            }

            finally

            {

                FechaConexao(cn);

            }

        }

        public async Task<DataTable> RetornaDataTableAsync(string strQuery)

        {

            SqlConnection cn = new SqlConnection();

            try

            {

                cn = new SqlConnection(conexao);

                await cn.OpenAsync();

                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = strQuery;

                cmd.CommandType = CommandType.Text;

                cmd.CommandTimeout = 1080;

                cmd.Connection = cn;
                var dataAdapter = new SqlDataAdapter();

                var table = new DataTable();

                dataAdapter.SelectCommand = cmd;

                dataAdapter.Fill(table);



                return table;

            }

            catch (Exception ex)

            {

                throw ex;

            }

            finally

            {

                FechaConexao(cn);

            }

        }

        public SqlDataReader RetornaDataReader(string strQuery)

        {

            SqlConnection cn = new SqlConnection();

            try

            {

                cn = AbreConexao();

                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = strQuery.ToString();

                cmd.CommandType = CommandType.Text;

                cmd.Connection = cn;

                return cmd.ExecuteReader();

            }

            catch (Exception ex)

            {

                throw ex;

            }

            finally

            {

                FechaConexao(cn);

            }

        }

        public string FiltroConveniadas(List<string> conveniadas, string templateFiltroEscola)

        {

            var filtroEscola = "";
            var coligada = "";
            var filial = "";
            var conveniada = "";

            string[] stringQuebrada;

            //Modelo de cada posição do array: [coligada,filial,conveniada]

            for (int i = 0; i < conveniadas.Count(); i++)

            {

                stringQuebrada = conveniadas[i].Split(',');

                coligada = stringQuebrada[0];

                filial = stringQuebrada[1];

                conveniada = stringQuebrada[2];



                filtroEscola += string.Format(templateFiltroEscola, coligada, filial, conveniada);

            }



            return filtroEscola.Remove(filtroEscola.Length - 3, 3);

        }

        public string FiltroColigadas(List<string> coligadas)
        {
            var filtroColigada = "";
            foreach (var codColigada in coligadas)
                filtroColigada += codColigada + ", ";
            return filtroColigada.Remove(filtroColigada.Length - 2, 2);
        }

    }
}
