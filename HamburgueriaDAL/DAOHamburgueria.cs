﻿using System;
using System.Data;

namespace HamburgueriaDAL
{
    public class DAOHamburgueria : DAO
    {
            public DAOHamburgueria(string stringConnection)
                : base(stringConnection)
            {
            }

            public DataTable Pedido(int id)
            {
                string sqlQuery = string.Format(@"
use Hamburgueria
SELECT p.id
		,P.Data
		,u.Nome
		,pp.Id IdProdutoDoPedido
		,pdt.Nome Produto
		--,a.Nome
		,P.ValorTotal
		,pp.Observacoes
		,e.Endereco
		,e.Complemento
		,b.Bairro
		--,*
  FROM Pedidos p
  join ProdutosDosPedidos pp on p.id = pp.IdPedido
  join Produtos pdt on pdt.id = pp.IdProduto
  left join AdicionaisDosProdutosDoPedido app on pp.Id =app.IdProdutoDoPedido
  left join Adicionais a on a.Id = app.IdAdicional
  join Usuarios u on u.Id = p.IdUsuario
  join Enderecos e on e.Id = p.IdEndereco
  join Bairros b on b.Id = e.IdBairro
  where p.id = {0}
  group by p.id
		,P.Data
		,u.Nome
		,pp.Id 
		,pdt.Nome
		--,a.Nome
		,P.ValorTotal
		,pp.Observacoes
		,e.Endereco
		,e.Complemento
		,b.Bairro", id);

                return RetornaDataTable(sqlQuery);
            }

        }
    }

